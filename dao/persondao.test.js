
var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
    host: "mysql.stud.iie.ntnu.no",
    user: "bragew",
    password: "AoQBGF9d",
    database: "bragew",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);


beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});


test("update person in db", done => {
  console.log("1");
    personDao.getOne(1, (status, old_data) => {
      personDao.updateOne(1, { navn: old_data[0].navn, alder: old_data[0].alder + 1, adresse: old_data[0].adresse }, (status, data) => {
        personDao.getOne(1, (status, new_data) => {
          expect(old_data[0].navn).toEqual(new_data[0].navn);
          expect(old_data[0].alder).toBe(new_data[0].alder);
          expect(old_data[0].adresse).toEqual(new_data[0].adresse);
          done();
        });
      } );
    });
});

test("delete person in db", done => {
  console.log("2")
    personDao.getAll((status, old_data) => {
        personDao.deleteOne(2, (status, data) => {
            personDao.getAll((status, new_data) => {
              console.log(new_data.length);
              expect(new_data.length).toBeLessThan(old_data.length);
              done();
            });
        });
    });
});

// testit in